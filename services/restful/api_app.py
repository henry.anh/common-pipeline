import logging
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_health import health

from config.config import Config


class ApiApp:

    def __init__(self, config: Config):

        self.config = config
        self.app = FastAPI()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.apply_cors()
        self.app.add_api_route("/health", health([self.health_check]), methods=["GET"])

    def apply_cors(self):
        self.app.add_middleware(CORSMiddleware,
                                allow_origins=["*"],
                                allow_credentials=True,
                                allow_methods=["*"],
                                allow_headers=["*"])

    @staticmethod
    def health_check(session: bool = True):
        return session

    def start(self, n_workers: int = 1):
        uvicorn.run(
            app=self.app,
            host=self.config.api_host,
            port=self.config.api_port,
            reload=False, log_level="debug",
            debug=False, workers=n_workers, factory=False,
            loop="asyncio", timeout_keep_alive=120
        )