import logging

from config.config import Config


class Singleton(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)

        return cls._instances[cls]


class BaseSingleton(metaclass=Singleton):
    pass


class BaseServiceSingleton(BaseSingleton):

    def __init__(self, config: Config):

        self.config = config
        self.logger = logging.getLogger(name=self.__class__.__name__)



