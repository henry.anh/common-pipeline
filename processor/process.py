import asyncio

from config.config import Config
from services.base_service import BaseServiceSingleton
from pipeline.pipeline import Pipeline
from data_model.data_model import BaseDataModel
# from modules.text_search.text_search_module import TextSearchModule
from modules.summing.summing_module import SummingModule


class Processor(BaseServiceSingleton):

    def __init__(self, config: Config = None):
        super(Processor, self).__init__(config=config)

        self.components = [
            # {TextSearchModule.__name__: (0, TextSearchModule.process)}
            {SummingModule.__name__: (0, SummingModule.sum_process)}
        ]

        self.pipeline = Pipeline(components=self.components, config=config)
        self.error_ids = []

    async def process(self, input_data: BaseDataModel):
        bill_id = input_data.index
        result = None
        if self.components:
            self.pipeline.message_queue.put_data(
                data=input_data,
                queue_name=list(self.components[0].keys())[-1]
            )
            [await controller.run() for controller in self.pipeline.controllers]
            count = 0
            output_data = None
            while True:
                if count > 1000:
                    self.error_ids.append(bill_id)
                    print("=" * 100 + f"\nLength errors: {len(self.error_ids)}")
                    break
                if len(self.error_ids) > 5:
                    for _id in self.error_ids:
                        if _id in self.pipeline.results:
                            del self.pipeline.results[_id]
                    self.error_ids = []

                count += 1
                if self.pipeline.results:
                    output_data = self.pipeline.results.get(bill_id, None)
                    if output_data is not None:
                        if bill_id in self.pipeline.results:
                            del self.pipeline.results[bill_id]
                        self.logger.error(f"Error id: {len(self.error_ids)}")
                        output_data.index = bill_id
                        break
                await asyncio.sleep(0.001)
            result = output_data
        return result
