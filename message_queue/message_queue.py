from multiprocessing import Queue


class MessageQueue:

    queues = {}

    @classmethod
    def init_queue(cls, components):
        for i, component in enumerate(components):
            if i == len(components) - 1:
                module = list(component.keys())[-1]
                cls.queues[module] = ("output", Queue())
                break
            module = list(component.keys())[-1]
            next_module = list(components[i + 1].keys())[-1]
            cls.queues[module] = (next_module, Queue())

    @classmethod
    def put_data(cls, data, queue_name: str):
        cls.queues[queue_name][1].put(data)

    @classmethod
    def get_data(cls, queue_name: str):
        return cls.queues[queue_name][1].get()

    @classmethod
    def queue_size(cls, queue_name: str):
        return cls.queues[queue_name][1].qsize()
