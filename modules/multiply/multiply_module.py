from typing import Dict, Any

from message_queue.message_queue import MessageQueue


class MultiplyModule:

    @classmethod
    def multiply_process(cls, message_queue: MessageQueue, kwargs):
        data = message_queue.get_data(queue_name=cls.__name__)

        return data