from typing import Dict, Any

from message_queue.message_queue import MessageQueue
from data_model.data_model import BaseDataModel
from modules.subtract.subtract.subtract import *


class SubtractModule:

    @classmethod
    def subtract_process(cls, message_queue: MessageQueue, kwargs):
        next_message_queue: str = message_queue.queues[cls.__name__][0]
        data: BaseDataModel = message_queue.get_data(queue_name=cls.__name__)
        calculate()
        message_queue.put_data(data=data, queue_name=next_message_queue)