from sys import getsizeof


def calculate():
    num = 1024 * 1024 * 8
    array = [i for i in range(num)]
    print(f"Size of array: {getsizeof(array)/1024/1024}")
