from typing import Dict, Any

from message_queue.message_queue import MessageQueue


class SummingModule:

    @classmethod
    def sum_process(cls, message_queue: MessageQueue, kwargs):
        next_message_queue: str = message_queue.queues[cls.__name__][0]
        data = message_queue.get_data(queue_name=cls.__name__)
        print("*"*100)
        for idx, value in enumerate(data.data):
            count = 0
            count += sum([i for i in range(100000+value)])
            data.data[idx] = str(value)
        print("+" * 100)
        # message_queue.put_data(data=data, queue_name=next_message_queue)
        return data