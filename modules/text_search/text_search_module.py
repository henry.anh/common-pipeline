from config.config import Config
from message_queue.message_queue import MessageQueue
from storages.elasticsearch.elasticsearch_service import ElasticSearchService

from data_model.es_data_model import ESDataModel


class TextSearchModule:
    _config: Config = Config()
    es: ElasticSearchService = ElasticSearchService(config=_config)

    @classmethod
    def process(cls, message_queue: MessageQueue, kwargs):
        next_message_queue: str = message_queue.queues[cls.__name__][0]
        data: ESDataModel = message_queue.get_data(queue_name=cls.__name__)
        responses = cls.es.get_document(
            index=data.es_index,
            text="quần kaki",
            top_k=data.top_k,
        )
        data.data["responses"] = responses
        return data
