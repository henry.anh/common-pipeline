import logging
from typing import List, Any, Dict

from config.config import Config
from services.base_service import BaseServiceSingleton
from controller.controller import Controller
from executor.executor import Executor
from message_queue.message_queue import MessageQueue

logging.basicConfig(level=logging.INFO)


class Pipeline(BaseServiceSingleton):

    def __init__(self, components: List[Dict[str, Any]], config: Config = None):
        super(Pipeline, self).__init__(config=config)

        self.results = {}
        self.config = config
        self.message_queue = MessageQueue
        self.message_queue.init_queue(components=components)
        self.controllers = self.init_controller(components=components)
        self.logger.info(f"Controller: {len(self.controllers)}")

    def init_controller(self, components: List[Dict[str, Any]]) -> List[Controller]:
        """
        Initialize the controller to ready to process the pipeline
        params: components: The list of dictionary which contains
        `key` represents for the kind of workers - Process or Thread
        `value` represents for the function for that worker
        """
        controllers = []
        for component in components:
            executor_type, func = list(component.values())[-1]
            controller = Controller(
                executor=Executor(executor_type=executor_type, config=self.config),
                func=func,
                message_queue=self.message_queue,
                results=self.results,
            )
            controllers.append(controller)

        return controllers


