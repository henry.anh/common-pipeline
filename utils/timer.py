from time import perf_counter


def timer(func):
    def wrapper(*args, **kwargs):
        s = perf_counter()
        res = func(*args, **kwargs)
        print(f"Elapsed time: {perf_counter() - s}")
        return res

    return wrapper
