import os
from typing import List

from common.common_keys import *


class Config:

    def __init__(self,
                 api_port: int = None,
                 mongo_host: str = None,
                 mongo_port: int = None,
                 mongo_username: str = None,
                 mongo_password: str = None,
                 mongo_replicaset_name: str = None,
                 mongo_uri: str = None,
                 mongo_database: str = None,
                 mongo_collection: str = None,
                 es_host: List[str] = None,
                 es_username: str = None,
                 es_password: str = None,
                 es_cert_path: str = None
                 ):

        self.api_host = "0.0.0.0"
        self.api_port = api_port if api_port is not None else int(os.getenv(API_PORT, 8080))

        self.mongo_host = mongo_host if mongo_host is not None else os.getenv(MONGO_HOST)
        self.mongo_username = mongo_username if mongo_username is not None else os.getenv(MONGO_USERNAME)
        self.mongo_password = mongo_password if mongo_password is not None else os.getenv(MONGO_PASSWORD)
        self.mongo_replicaset_name = mongo_replicaset_name \
            if mongo_replicaset_name is not None else os.getenv(MONGO_REPLICASET_NAME)
        self.mongo_port = mongo_port if mongo_port is not None else int(os.getenv(MONGO_PORT, 27017))
        _mongo_port = self.mongo_port
        if not _mongo_port or self.mongo_port is None:
            self.mongo_uri = f"mongodb+srv://{self.mongo_username}:{self.mongo_password}@{self.mongo_host}/" \
                             f"admin?authMechanism=SCRAM-SHA-1&authSource=admin&ssl=false&tls=false"
            if self.mongo_replicaset_name:
                self.mongo_uri = f"{self.mongo_uri}&replicaSet={self.mongo_replicaset_name}"
        else:
            self.mongo_uri = mongo_uri

        self.mongo_database = mongo_database if mongo_database is not None else os.getenv(
            MONGO_DATABASE
        )
        self.mongo_collection = mongo_collection if mongo_collection is not None else os.getenv(
            MONGO_COLLECTION
        )

        self.es_host = es_host if es_host is not None else os.getenv(
            ES_HOST,
            ["172.29.32.24:35300", "172.29.32.24:35301", "172.29.32.24:35302"])
        self.es_username = es_username if es_username is not None else os.getenv(ES_USERNAME)
        self.es_password = es_password if es_password is not None else os.getenv(ES_PASSWORD)
        self.es_cert_path = es_cert_path if es_cert_path is not None else os.getenv(ES_CERT_PATH)

        self.num_process_workers = int(os.getenv(MAX_PROCESS_WORKERS, 3))
        self.num_thread_workers = int(os.getenv(MAX_THREAD_WORKERS, 10))
