from datetime import datetime
from typing import Optional, Any
from pydantic import BaseModel

from common.common_keys import *


class BaseMetaData(BaseModel):

    processing_time: Optional[float]
    receive_time: Optional[float]
    response_time: Optional[float]

    def __init__(self,
                 processing_time: float = None,
                 receive_time: float = None,
                 response_time: float = None
                 ):
        super(BaseMetaData, self).__init__(
            processing_time=processing_time,
            receive_time=receive_time,
            response_time=response_time
        )

    def update_receive_time(self):
        if self.receive_time is None:
            self.receive_time = datetime.now().timestamp()

    def update_response_time(self):
        self.response_time = datetime.now().timestamp()
        self.processing_time = self.response_time - self.receive_time

    @staticmethod
    def from_json(data):
        return BaseMetaData(**data)

    def dict(self, *args, **kwargs):
        return {
            PROCESSING_TIME: self.processing_time,
            RECEIVE_TIME: self.receive_time,
            RESPONSE_TIME: self.response_time
        }


class BaseDataModel(BaseModel):

    index: str
    data: Any
    metadata: Optional[BaseMetaData]

    def update_receive_time(self):
        self.metadata.update_receive_time()

    def update_response_time(self):
        self.metadata.update_response_time()

    @staticmethod
    def from_json(data):
        meta_data = data.get(METADATA, {})
        meta_data = BaseMetaData.from_json(data=meta_data)
        data[METADATA] = meta_data
        return BaseDataModel(**data)
