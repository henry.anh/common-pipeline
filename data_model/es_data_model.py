from data_model.data_model import BaseDataModel


class ESDataModel(BaseDataModel):

    mode: str
    es_index: str
    top_k: int
