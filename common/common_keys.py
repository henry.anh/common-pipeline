API_PORT = "API_PORT"
MONGO_HOST = "MONGO_HOST"
MONGO_PORT = "MONGO_PORT"
MONGO_USERNAME = "MONGO_USERNAME"
MONGO_PASSWORD = "MONGO_PASSWORD"
MONGO_REPLICASET_NAME = "MONGO_REPLICASET_NAME"
MONGO_DATABASE = "MONGO_DATABASE"
MONGO_COLLECTION = "MONGO_COLLECTION"
ES_HOST = "ES_HOST"
ES_USERNAME = "ES_USERNAME"
ES_PASSWORD = "ES_PASSWORD"
ES_CERT_PATH = "ES_CERT_PATH"
MAX_PROCESS_WORKERS = "MAX_PROCESS_WORKERS"
MAX_THREAD_WORKERS = "MAX_THREAD_WORKERS"

PROCESSING_TIME = "processing_time"
RECEIVE_TIME = "receive_time"
RESPONSE_TIME = "response_time"
METADATA = "metadata"

URL = "URL"
BODY = "BODY"
PARAMS = "PARAMS"
METHOD = "METHOD"

