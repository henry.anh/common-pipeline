import logging
import asyncio
from typing import Dict, Any
from time import perf_counter

from executor.executor import Executor
from message_queue.message_queue import MessageQueue

logging.basicConfig(level=logging.INFO)


class Controller:

    def __init__(self, executor: Executor, func,
                 message_queue: MessageQueue,
                 results: Dict[str, Any],
                 **kwargs,
                 ):
        self.executor = executor
        self.func = func
        self.message_queue = message_queue
        self.results = results
        self.kwargs = kwargs

        self.logger = logging.getLogger(self.__class__.__name__)

    async def run(self):
        start_time = perf_counter()
        event_loop = asyncio.get_event_loop()
        if self.executor.executor_type == 0:
            futures = event_loop.run_in_executor(
                 self.executor.executor,
                 self.func,
                 self.message_queue,
                 self.kwargs
            )
        else:
            futures = event_loop.run_in_executor(
                self.executor.executor,
                self.func,
                self.message_queue,
                self.kwargs
            )
        results = await futures
        if results is not None:
            index = results.index
            self.results[index] = results

        self.logger.info(f"{self.func.__name__.upper()} - Controller - Elapsed time: {perf_counter() - start_time}")
