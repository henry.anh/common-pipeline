from pymongo import MongoClient
from typing import Any, Dict

from config.config import Config
from services.base_service import BaseServiceSingleton
from storages.mongo.structure import *


class MongoService(BaseServiceSingleton):

    def __init__(self, config: Config):
        super(MongoService, self).__init__(config=config)

        self.config = config
        self.mongo_client = self.init_mongo_client(config=config)

        self.get_mongo = GetMongo
        self.put_mongo = PutMongo
        self.delete_mongo = DeleteMongo

    @staticmethod
    def init_mongo_client(config: Config):
        if config.mongo_uri is not None:
            return MongoClient(config.mongo_uri)
        else:
            return MongoClient(
                host=config.mongo_host,
                port=config.mongo_port,
                username=config.mongo_username,
                password=config.mongo_password
            )

    def _get_collection(self, database_name: str, collection_name: str):
        return self.mongo_client[database_name][collection_name]

    def get_collection(self, database_name: str, collection_name: str):
        return self._get_collection(
            database_name=database_name,
            collection_name=collection_name
        )

    def _get_data(self, database_name: str, collection_name: str, query: Dict[str, Any]):
        collection = self.get_collection(
            database_name=database_name,
            collection_name=collection_name
        )
        query = self.get_mongo.get(query=query)
        return collection.find(query)

    def get_data(self, database_name: str, collection_name: str, query: Dict[str, Any]):
        return self._get_data(
            database_name=database_name,
            collection_name=collection_name,
            query=query
        )

    def _put_data(self, database_name: str, collection_name: str, upsert: bool = True):
        collection = self.get_collection(
            database_name=database_name,
            collection_name=collection_name
        )
        query, updated_query = self.put_mongo.put()
        return collection.update_one(query, update=updated_query, upsert=upsert)

    def put_data(self, database_name: str, collection_name: str, upsert: bool = True):
        return self._put_data(
            database_name=database_name,
            collection_name=collection_name,
            upsert=upsert
        )

    def _delete_data(self, database_name: str, collection_name: str):
        collection = self.get_collection(
            database_name=database_name,
            collection_name=collection_name
        )
        query = self.delete_mongo.delete()
        return collection.delete_one(query)

    def delete_data(self, database_name: str, collection_name: str):
        return self._delete_data(
            database_name=database_name,
            collection_name=collection_name
        )
