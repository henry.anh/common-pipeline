from storages.elasticsearch.query.post.utils import generate_id

class PostElasticSearch:

    @staticmethod
    def post_data(index: str, text: str):

        return {
            "_op_type": "index",
            "_index": index,
            "_id": generate_id(text=text)
        }