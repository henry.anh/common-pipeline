from typing import Dict, Any


class PutIndexElasticSearch:

    @staticmethod
    def put_prefix(index: str) -> Dict[str, Any]:
        return {
            "mappings": {
                "properties": {
                    index: {
                        "type": "keyword",
                        "fields": {
                            "analyzed_title": {
                                "type": "text"
                            }
                        }
                    }
                }
            }
        }

    @staticmethod
    def put_edge_ngram(index: str) -> Dict[str, Any]:
        return {
            "settings": {
                "analysis": {
                    "analyzer": {
                        "custom_edge_ngram_tokenizer": {
                            "type": "custom",
                            "tokenizer": "customized_edge_tokenizer",
                            "filter": [
                                "lowercase"
                            ]
                        }
                    },
                    "tokenizer": {
                        "customized_edge_tokenizer": {
                            "type": "edge_ngram",
                            "min_gram": 2,
                            "max_gram": 10,
                            "token_chars": [
                                "letter",
                                "digit"
                            ]
                        }
                    }
                }
            },
            "mappings": {
                "properties": {
                    index: {
                        "type": "text",
                        "analyzer": "custom_edge_ngram_tokenizer"
                    }
                }
            }
        }