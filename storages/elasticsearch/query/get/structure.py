from typing import Dict, Any


class GetElasticSearch:

    @staticmethod
    def get_config(text: str) -> Dict[str, Any]:
        return {
            "query": {
                "match": {
                    "message": {
                        "query": text
                    }
                }
            }
        }
