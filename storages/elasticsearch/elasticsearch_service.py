from elasticsearch import AsyncElasticsearch, Elasticsearch

from config.config import Config
from storages.elasticsearch.query.get.structure import GetElasticSearch


class AsyncElasticSearchService:

    def __init__(self, config: Config):

        self.es = self.connect_elasticsearch(config=config)

        self.get_elasticsearch = GetElasticSearch

    async def get_document(self, index: str, text: str, top_k: int):
        body = self.get_elasticsearch.get_config(text=text)
        data = await self.es.search(index=index, body=body, size=top_k)
        return data

    @staticmethod
    def connect_elasticsearch(config: Config) -> AsyncElasticsearch:
        if config.es_username and config.es_password:
            return AsyncElasticsearch(
                hosts=config.es_host,
                http_auth=(config.es_username, config.es_password),
                ca_certs=config.es_cert_path,
                timeout=30,
                max_retries=10,
                retry_on_timeout=True
            )
        else:
            return AsyncElasticsearch(
                hosts=config.es_host,
                timeout=30,
                max_retries=10,
                retry_on_timeout=True
            )


class ElasticSearchService:

    def __init__(self, config: Config):

        self.es = self.connect_elasticsearch(config=config)

        self.get_elasticsearch = GetElasticSearch

    def get_document(self, index: str, text: str, top_k: int):
        body = self.get_elasticsearch.get_config(text=text)
        data = self.es.search(index=index, body=body, size=top_k)
        return data

    @staticmethod
    def connect_elasticsearch(config: Config) -> Elasticsearch:
        if config.es_username and config.es_password:
            return Elasticsearch(
                hosts=config.es_host,
                http_auth=(config.es_username, config.es_password),
                ca_certs=config.es_cert_path,
                timeout=30,
                max_retries=10,
                retry_on_timeout=True
            )
        else:
            return Elasticsearch(
                hosts=config.es_host,
                timeout=30,
                max_retries=10,
                retry_on_timeout=True
            )
