from typing import Union
from concurrent.futures.thread import ThreadPoolExecutor
from concurrent.futures.process import ProcessPoolExecutor

from config.config import Config


class Executor:

    def __init__(self, executor_type: str, config: Config):

        self.num_process_workers = config.num_process_workers
        self.num_thread_workers = config.num_thread_workers
        self.executor_type = executor_type

        self.executor = self.init_executor()

    def init_executor(self) -> Union[ProcessPoolExecutor, ThreadPoolExecutor]:
        """
        params: components: The list of dictionary which contains
        `key` represents for the kind of workers - Process or Thread
        `value` represents for the function for that worker
        """
        if self.executor_type == 0:
            return ThreadPoolExecutor(max_workers=self.num_thread_workers)
        else:
            return ProcessPoolExecutor(max_workers=self.num_process_workers)
