import psutil
import asyncio
from concurrent.futures import ThreadPoolExecutor
from fastapi import Response
from typing import Dict
import prometheus_client as prom
from prometheus_client import CollectorRegistry
from time import perf_counter
from fastapi.responses import JSONResponse

from config.config import Config
from processor.process import Processor
from services.restful.api_app import ApiApp
from data_model.data_model import BaseDataModel


async def get_total_cpu_memory_by_process() -> Dict[str, float]:
    """
    Get CPU, RAM by processes
    :return:
    """

    def _get_cpu_men() -> Dict[str, float]:
        output = {}
        for proc in psutil.process_iter():
            mem = proc.memory_info().rss / 1024 ** 2
            cpu = proc.cpu_percent()
            output["RAM"] = output.get("RAM", 0) + mem
            output["CPU"] = output.get("CPU", 0) + cpu
        return output

    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(executor=ThreadPoolExecutor(max_workers=1), func=_get_cpu_men)


class App(ApiApp):

    def __init__(self, config: Config):
        super(App, self).__init__(config=config)

        self.processor = Processor(config=config)
        self.registry = CollectorRegistry()
        self.system_usage = prom.Gauge(
            name='system_usage',
            documentation='Hold current system resource usage',
            labelnames=['resource_type'],
            registry=self.registry
        )
        self.metrics = {
            "GlobalCPU": psutil.cpu_percent,
            "GlobalRAM": lambda: psutil.virtual_memory()[2]
        }
        self.init_metrics()

        @self.app.get("/health", tags=["Baseline"], description="Health Check")
        async def health():
            return JSONResponse(
                status_code=200,
                content=200
            )

        @self.app.post("/process", tags=["Baseline"], description="Processing body")
        async def process(input_data: BaseDataModel) -> JSONResponse:
            # input_data = await self.processor.process(input_data=input_data)
            # return input_data
            try:
                start_time = perf_counter()
                input_id = input_data.index
                output_data = await self.processor.process(input_data=input_data)
                output_id = output_data.index if output_data else None
                self.logger.info(f"Elapsed time of Pipeline: {perf_counter() - start_time}")
                if input_id == output_id:
                    return JSONResponse(
                        status_code=200,
                        content=output_data.dict()
                    )
                else:
                    return JSONResponse(
                        status_code=503,
                        content=f"FALSE -{input_id} - {output_id}"
                    )
            except Exception as e:
                return JSONResponse(
                    status_code=500,
                    content=e
                )

    def init_metrics(self):
        @self.app.get("/metrics", tags=["Metrics"])
        async def metrics():
            for label, scrapper_function in self.metrics.items():
                self.system_usage.labels(label).set(scrapper_function())
            process_info = await get_total_cpu_memory_by_process()
            for metric, value in process_info.items():
                self.system_usage.labels(metric).set(value)
            return Response(
                content=prom.generate_latest(self.registry),
                media_type="text/plain"
            )


def create_app():
    _config = Config(api_port=8082)
    _app = App(config=_config)
    return _app


main_app = create_app()
app = main_app.app




